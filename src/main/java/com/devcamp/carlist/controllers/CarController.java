package com.devcamp.carlist.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarController {

	@GetMapping("/")
	public ArrayList<String> index() {
		ArrayList<String> carlist = new ArrayList<>();

		carlist.add("Vinfast");
		carlist.add("Toyota");
		carlist.add("BMW");
		carlist.add("Mitsubishi");

		return carlist;
	}

}